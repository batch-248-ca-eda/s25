console.log("Hello");

/*
	JSON
			JSON stans for JavasScript Object notation
			JSON is also used in other programming

*/


		let sample1 = `
			{
				"name": "Cardo Dalisay",
				"age":20,
				"address": {
					"city": "Quezon City",
					"country": "Philippines"
				}
			}

		`;

		console.log(sample1);

		// Create a variable that will hold a "JSON" and createa person object

		let person = `
			{
				"Name": "Japeth",
				"age": 24,
				"address": {
					"city": "Tacloban",
					"country": "Philippines"
				}
	
			}


		`;

		console.log(person);
		console.log(typeof person);//string

		// Are we able to turn a JSON into a JS Object?
		// JSON.parse() - will return the JSON as an object

		console.log(JSON.parse(sample1));
		console.log(JSON.parse(person));

		// JSON Array
		// JSON array is an array of JSON

		let sampleArr = `
			[
				{
					"email": "sinayangmoako@gmail.com",
					"password": "february14",
					"isAdmin": false
				},
				{
					"email": "dalisay@cardo.com",
					"password": "thecountryman",
					"isAdmin": false
				},
				{
					"email": "jsonv@gmail.com",
					"password": "friday13",
					"isAdmin": false
				}

			]

		`;

		console.log(sampleArr);
		console.log(typeof sampleArr);

		// Can we use Array Methods on a JSON Array?
		// No. Because a JSON is a string

		// so what can we do to be able to add more items/objects into our sampleArr?

		// PARSE the JSON array to a JS array and save it in a variable

		let parsedSampleArr = JSON.parse(sampleArr);
		console.log([parsedSampleArr]);
		console.log(typeof parsedSampleArr);//object - array

		// can we now delete the last item in the JSON Array?

		console.log(parsedSampleArr.pop());
		console.log(parsedSampleArr);

		// if for example we need to send this data back to our client/front end it shoulb be in JSON format

		// JSON.parse() does not mutate or update the original JSON
		// We can actually turn a JS object into a JSON
		// JSON.stringify() - this will stringify JS objects as JSON

		sampleArr = JSON.stringify(parsedSampleArr);

		console.log(typeof sampleArr);
		console.log(sampleArr);

		// Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => frontend/client

		/*
			Mini Activity
			Given the JSON array, process it and convert to a JS Object so wen can manipulate the array

			Delete the last item in the array and add a new item in the array

			Stringify the array back in JSON

			and update jsonArr with the stringified array


		*/


		console.warn("Mini Activity Start");

		let jsonArr = `

			[
				"pizza",
				"hamburger",
				"spaghetti",
				"shanghai",
				"hotdog stick on a pineapple",
				"pancit bihon"



			]

		`
		console.log(typeof jsonArr);
		console.log(jsonArr);


		let sampJSONArr = JSON.parse(jsonArr);
		console.log(typeof sampJSONArr)

		sampJSONArr.pop();
		sampJSONArr.push("Calamares");

		jsonArr = JSON.stringify(sampJSONArr);
		console.log(jsonArr);
		console.log(typeof jsonArr);



		// Gather User Details

		let firstName = prompt("What is your name?");
		let lastName = prompt("What is your last name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country: prompt("Which country does your city address belong to?")
		};

		let otherData = JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				age: age,
				address: address
		});

		console.log(otherData);